# Collaborative Coder Coalition

VERSION 0.7.1 - July 2017

[![slack](https://img.shields.io/badge/chat-on%20slack-blue.svg)](https://screeps.slack.com/messages/ccc/)

We are also forming a [screeps game](https://screeps.com) player organization, the Collaborative Coder Coalition [CCC].
Please contact us on [slack](https://screeps.slack.com/messages/ccc) if you want to join the organization.
If you don't have a screeps slack account yet, please click [here](http://chat.screeps.com/) to invite yourself, then join channel #ccc.

[Public Repository](https://gitlab.com/ScreepsCCC/public)
[Wiki](https://gitlab.com/ScreepsCCC/public/wikis/home)
[CCC Constitution](https://gitlab.com/ScreepsCCC/public/blob/dev/CONSTITUTION.md)
[CCC on slack](https://screeps.slack.com/messages/ccc)
